<?php

if (!defined('K_INSTALLATION_IN_PROGRESS')) die(); // cannot be loaded directly

header('Content-Type: text/html; charset=' . K_CHARSET);

require_once(K_COUCH_DIR . 'auth/auth.php');
require_once(K_COUCH_DIR . 'parser/parser.php');
require_once(K_COUCH_DIR . 'parser/HTMLParser.php');
require_once(K_COUCH_DIR . 'page.php');
require_once(K_COUCH_DIR . 'tags.php');

$TAGS = new KTags();
$CTX = new KContext();

$k_couch_tables = array(
	K_TBL_TEMPLATES,
	K_TBL_FIELDS,
	K_TBL_PAGES,
	K_TBL_FOLDERS,
	K_TBL_USERS,
	K_TBL_USER_LEVELS,
	K_TBL_SETTINGS,
	K_TBL_DATA_TEXT,
	K_TBL_DATA_NUMERIC,
	K_TBL_FULLTEXT,
	K_TBL_COMMENTS,
	K_TBL_RELATIONS,
);
$k_stmts = array();
$k_stmts[] = "CREATE TABLE " . K_TBL_COMMENTS . " (
	id        int AUTO_INCREMENT NOT NULL,
	tpl_id    int NOT NULL,
	page_id   int NOT NULL,
	user_id   int,
	name      tinytext,
	email     varchar(128),
	link      varchar(255),
	ip_addr   varchar(100),
	date      datetime,
	data      text,
	approved  tinyint DEFAULT '0',
	PRIMARY KEY (id)
	) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;";

$k_stmts[] = "CREATE TABLE " . K_TBL_DATA_NUMERIC . " (
		page_id   int NOT NULL,
		field_id  int NOT NULL,
		value     decimal(65,2) DEFAULT '0.00',
		PRIMARY KEY (page_id, field_id)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;";

$k_stmts[] = "CREATE TABLE " . K_TBL_DATA_TEXT . " (
			page_id       int NOT NULL,
			field_id      int NOT NULL,
			value         longtext,
			search_value  text,
			PRIMARY KEY (page_id, field_id)
			) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;";

$k_stmts[] = "CREATE TABLE " . K_TBL_FIELDS . " (
				id                int AUTO_INCREMENT NOT NULL,
				template_id       int NOT NULL,
				name              varchar(255) NOT NULL,
				label             varchar(255),
				k_desc            varchar(255),
				k_type            varchar(128) NOT NULL,
				hidden            int(1),
				search_type       varchar(20) DEFAULT 'text',
				k_order           int,
				data              longtext,
				default_data      longtext,
				required          int(1),
				deleted           int(1),
				validator         varchar(255),
				validator_msg     text,
				k_separator       varchar(20),
				val_separator     varchar(20),
				opt_values        text,
				opt_selected      tinytext,
				toolbar           varchar(20),
				custom_toolbar    text,
				css               text,
				custom_styles     text,
				maxlength         int,
				height            int,
				width             int,
				k_group           varchar(128),
				collapsed         int(1) DEFAULT '-1',
				assoc_field       varchar(128),
				crop              int(1) DEFAULT '0',
				enforce_max       int(1) DEFAULT '1',
				quality           int,
				show_preview      int(1) DEFAULT '0',
				preview_width     int,
				preview_height    int,
				no_xss_check      int(1) DEFAULT '0',
				rtl               int(1) DEFAULT '0',
				body_id           tinytext,
				body_class        tinytext,
				disable_uploader  int(1) DEFAULT '0',
				_html             text COMMENT 'Internal',
				dynamic           text,
				custom_params     mediumtext,
				searchable        int(1) DEFAULT '1',
				class             tinytext,
				not_active        text,
				PRIMARY KEY (id)
				) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;";

$k_stmts[] = "CREATE TABLE " . K_TBL_FOLDERS . " (
					id            int AUTO_INCREMENT NOT NULL,
					pid           int DEFAULT '-1',
					template_id   int NOT NULL,
					name          varchar(255) NOT NULL,
					title         varchar(255),
					k_desc        mediumtext,
					image         text,
					access_level  int DEFAULT '0',
					weight        int DEFAULT '0',
					PRIMARY KEY (id)
					) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;";

$k_stmts[] = "CREATE TABLE " . K_TBL_FULLTEXT . " (
						page_id  int NOT NULL,
						title    varchar(255),
						content  text,
						PRIMARY KEY (page_id)
						) ENGINE = MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;";

$k_stmts[] = "CREATE TABLE " . K_TBL_USER_LEVELS . " (
							id        int AUTO_INCREMENT NOT NULL,
							name      varchar(100),
							title     varchar(100),
							k_level   int DEFAULT '0',
							disabled  int DEFAULT '0',
							PRIMARY KEY (id)
							) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;";

$k_stmts[] = "CREATE TABLE " . K_TBL_PAGES . " (
								id                 int AUTO_INCREMENT NOT NULL,
								template_id        int NOT NULL,
								parent_id          int DEFAULT '0',
								page_title         varchar(255),
								page_name          varchar(255),
								creation_date      datetime DEFAULT '0000-00-00 00:00:00',
								modification_date  datetime DEFAULT '0000-00-00 00:00:00',
								publish_date       datetime DEFAULT '0000-00-00 00:00:00',
								status             int,
								is_master          int(1) DEFAULT '0',
								page_folder_id     int DEFAULT '-1',
								access_level       int DEFAULT '0',
								comments_count     int DEFAULT '0',
								comments_open      int(1) DEFAULT '1',
								nested_parent_id   int DEFAULT '-1',
								weight             int DEFAULT '0',
								show_in_menu       int(1) DEFAULT '1',
								menu_text          varchar(255),
								is_pointer         int(1) DEFAULT '0',
								pointer_link       text,
								pointer_link_detail text,
								open_external int(1) DEFAULT '0',
								masquerades          int(1) DEFAULT '0',
								strict_matching      int(1) DEFAULT '0',
								file_name            varchar(260),
								file_ext             varchar(20),
								file_size            int DEFAULT '0',
								file_meta            text,
								creation_IP          varchar(45),
								k_order            int DEFAULT '0',
								ref_count          int DEFAULT '1',

								PRIMARY KEY (id)
								) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;";

$k_stmts[] = "CREATE TABLE " . K_TBL_SETTINGS . " (
									k_key    varchar(255) NOT NULL,
									k_value  longtext,
									PRIMARY KEY (k_key)
									) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;";

$k_stmts[] = "CREATE TABLE " . K_TBL_TEMPLATES . " (
										id            int AUTO_INCREMENT NOT NULL,
										name          varchar(255) NOT NULL,
										description   varchar(255),
										clonable      int(1) DEFAULT '0',
										executable    int(1) DEFAULT '1',
										title         varchar(255),
										access_level  int DEFAULT '0',
										commentable   int(1) DEFAULT '0',
										hidden        int(1) DEFAULT '0',
										k_order       int DEFAULT '0',
										dynamic_folders   int(1) DEFAULT '0',
										nested_pages int(1) DEFAULT '0',
										gallery          int(1) DEFAULT '0',
										handler          text,
										custom_params    text,
										type             varchar(255),
										config_list      text,
										config_form      text,
										parent           varchar(255),
										icon             varchar(255),
										deleted          int(1) DEFAULT '0',
										has_globals      int(1) DEFAULT '0',
										PRIMARY KEY (id)
										) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;";

$k_stmts[] = "CREATE TABLE " . K_TBL_USERS . " (
											id                 int AUTO_INCREMENT NOT NULL,
											name               varchar(255) NOT NULL,
											title              varchar(255),
											password           varchar(64) NOT NULL,
											email              varchar(128) NOT NULL,
											activation_key     varchar(64),
											password_reset_key varchar(64),
											registration_date  datetime,
											access_level       int DEFAULT '0',
											disabled           int DEFAULT '0',
											`system`           int DEFAULT '0',
											last_failed        bigint(11) DEFAULT '0',
											failed_logins      int DEFAULT '0',
											PRIMARY KEY (id)
											) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;";

$k_stmts[] = "CREATE TABLE `" . K_TBL_RELATIONS . "` (
												`pid`     int NOT NULL,
												`fid`     int NOT NULL,
												`cid`     int NOT NULL,
												`weight`  int DEFAULT '0',
												PRIMARY KEY (`pid`, `fid`, `cid`)
											) ENGINE = InnoDB CHARACTER SET `utf8` COLLATE `utf8_general_ci`;";

$k_stmts[] = "CREATE TABLE `" . K_TBL_ATTACHMENTS . "` (
												`attach_id`       bigint(11) UNSIGNED AUTO_INCREMENT NOT NULL,
												`file_real_name`  varchar(255) NOT NULL,
												`file_disk_name`  varchar(255) NOT NULL,
												`file_extension`  varchar(255) NOT NULL,
												`file_size`       int(20) UNSIGNED NOT NULL DEFAULT '0',
												`file_time`       int(10) UNSIGNED NOT NULL DEFAULT '0',
												`is_orphan`       tinyint(1) UNSIGNED DEFAULT '1',
												`hit_count`       int(10) UNSIGNED DEFAULT '0',
												`creation_ip`     varchar(45),
												PRIMARY KEY (`attach_id`)
											) ENGINE = InnoDB CHARACTER SET `utf8` COLLATE `utf8_general_ci`;";

$k_stmts[] = "CREATE INDEX " . K_TBL_COMMENTS . "_Index01
											ON " . K_TBL_COMMENTS . "
											(date);";

$k_stmts[] = "CREATE INDEX " . K_TBL_COMMENTS . "_Index02
											ON " . K_TBL_COMMENTS . "
											(page_id, approved, date);";

$k_stmts[] = "CREATE INDEX " . K_TBL_COMMENTS . "_Index03
											ON " . K_TBL_COMMENTS . "
											(tpl_id, approved, date);";

$k_stmts[] = "CREATE INDEX " . K_TBL_COMMENTS . "_Index04
											ON " . K_TBL_COMMENTS . "
											(approved, date);";

$k_stmts[] = "CREATE INDEX " . K_TBL_COMMENTS . "_Index05
											ON " . K_TBL_COMMENTS . "
											(tpl_id, page_id, approved, date);";

$k_stmts[] = "CREATE INDEX " . K_TBL_DATA_NUMERIC . "_Index01
											ON " . K_TBL_DATA_NUMERIC . "
											(value);";

$k_stmts[] = "CREATE INDEX " . K_TBL_DATA_NUMERIC . "_Index02
											ON " . K_TBL_DATA_NUMERIC . "
											(field_id, value);";

$k_stmts[] = "CREATE INDEX " . K_TBL_DATA_TEXT . "_Index01
											ON " . K_TBL_DATA_TEXT . "
											(search_value(255));";

$k_stmts[] = "CREATE INDEX " . K_TBL_DATA_TEXT . "_Index02
											ON " . K_TBL_DATA_TEXT . "
											(field_id, search_value(255));";

$k_stmts[] = "CREATE INDEX " . K_TBL_FIELDS . "_index01
											ON " . K_TBL_FIELDS . "
											(k_group, k_order, id);";

$k_stmts[] = "CREATE INDEX " . K_TBL_FIELDS . "_Index02
											ON " . K_TBL_FIELDS . "
											(template_id);";

$k_stmts[] = "CREATE INDEX " . K_TBL_FOLDERS . "_Index01
											ON " . K_TBL_FOLDERS . "
											(template_id, id);";

$k_stmts[] = "CREATE UNIQUE INDEX " . K_TBL_FOLDERS . "_Index02
											ON " . K_TBL_FOLDERS . "
											(template_id, name(255));";

$k_stmts[] = "CREATE FULLTEXT INDEX " . K_TBL_FULLTEXT . "_Index01
											ON " . K_TBL_FULLTEXT . "
											(title);";

$k_stmts[] = "CREATE FULLTEXT INDEX " . K_TBL_FULLTEXT . "_Index02
											ON " . K_TBL_FULLTEXT . "
											(content);";

$k_stmts[] = "CREATE UNIQUE INDEX " . K_TBL_USER_LEVELS . "_index01
											ON " . K_TBL_USER_LEVELS . "
											(k_level);";

$k_stmts[] = "CREATE INDEX " . K_TBL_PAGES . "_Index01
											ON " . K_TBL_PAGES . "
											(template_id, publish_date);";

$k_stmts[] = "CREATE INDEX " . K_TBL_PAGES . "_Index02
											ON " . K_TBL_PAGES . "
											(template_id, page_folder_id, publish_date);";

$k_stmts[] = "CREATE UNIQUE INDEX " . K_TBL_PAGES . "_Index03
											ON " . K_TBL_PAGES . "
											(template_id, page_name(255));";

$k_stmts[] = "CREATE INDEX " . K_TBL_PAGES . "_Index04
											ON " . K_TBL_PAGES . "
											(template_id, modification_date);";

$k_stmts[] = "CREATE INDEX " . K_TBL_PAGES . "_Index05
											ON " . K_TBL_PAGES . "
											(template_id, page_folder_id, modification_date);";

$k_stmts[] = "CREATE INDEX " . K_TBL_PAGES . "_Index06
											ON " . K_TBL_PAGES . "
											(template_id, page_folder_id, page_name(255));";

$k_stmts[] = "CREATE INDEX " . K_TBL_PAGES . "_Index07
											ON " . K_TBL_PAGES . "
											(template_id, comments_count);";

$k_stmts[] = "CREATE INDEX " . K_TBL_PAGES . "_Index08
											ON " . K_TBL_PAGES . "
											(template_id, page_title(255));";

$k_stmts[] = "CREATE INDEX " . K_TBL_PAGES . "_Index09
											ON " . K_TBL_PAGES . "
											(template_id, page_folder_id, page_title(255));";

$k_stmts[] = "CREATE INDEX " . K_TBL_PAGES . "_Index10
											ON " . K_TBL_PAGES . "
											(template_id, page_folder_id, comments_count);";

$k_stmts[] = "CREATE INDEX " . K_TBL_PAGES . "_Index11
											ON " . K_TBL_PAGES . "
											(template_id, parent_id, modification_date);";

$k_stmts[] = "CREATE INDEX " . K_TBL_PAGES . "_Index12
											ON " . K_TBL_PAGES . "
											(parent_id, modification_date);";

$k_stmts[] = "CREATE INDEX " . K_TBL_PAGES . "_Index13
											ON " . K_TBL_PAGES . "
											(template_id, is_pointer, masquerades, pointer_link_detail(255));";

$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_Index14` ON `" . K_TBL_PAGES . "` (`template_id`, `file_name`(255));";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_Index15` ON `" . K_TBL_PAGES . "` (`template_id`, `page_folder_id`, `file_name`(255));";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_Index16` ON `" . K_TBL_PAGES . "` (`template_id`, `file_ext`(20), `file_name`(255));";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_Index17` ON `" . K_TBL_PAGES . "` (`template_id`, `page_folder_id`, `file_ext`(20), `file_name`(255));";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_Index18` ON `" . K_TBL_PAGES . "` (`template_id`, `file_size`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_Index19` ON `" . K_TBL_PAGES . "` (`template_id`, `page_folder_id`, `file_size`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_Index20` ON `" . K_TBL_PAGES . "` (`creation_IP`, `creation_date`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index21` ON `" . K_TBL_PAGES . "` (`template_id`, `k_order`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index22` ON `" . K_TBL_PAGES . "` (`template_id`, `page_folder_id`, `k_order`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index23` ON `" . K_TBL_PAGES . "` (`k_order`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_Index24` ON `" . K_TBL_PAGES . "` (`status`, `ref_count`, `modification_date`);";

$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index25` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `publish_date`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index26` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `page_name`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index27` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `page_title`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index28` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `modification_date`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index29` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `comments_count`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index30` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `file_name`(255));";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index31` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `file_ext`, `file_name`(255));";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index32` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `file_size`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index33` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `k_order`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index34` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `page_folder_id`, `publish_date`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index35` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `page_folder_id`, `page_name`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index36` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `page_folder_id`, `page_title`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index37` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `page_folder_id`, `modification_date`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index38` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `page_folder_id`, `comments_count`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index39` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `page_folder_id`, `file_name`(255));";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index40` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `page_folder_id`, `file_ext`, `file_name`(255));";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index41` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `page_folder_id`, `file_size`);";
$k_stmts[] = "CREATE INDEX `" . K_TBL_PAGES . "_index42` ON `" . K_TBL_PAGES . "` (`template_id`, `parent_id`, `page_folder_id`, `k_order`);";

$k_stmts[] = "CREATE UNIQUE INDEX " . K_TBL_TEMPLATES . "_Index01
											ON " . K_TBL_TEMPLATES . "
											(name);";

$k_stmts[] = "CREATE INDEX " . K_TBL_USERS . "_activation_key
											ON " . K_TBL_USERS . "
											(activation_key);";

$k_stmts[] = "CREATE INDEX " . K_TBL_USERS . "_password_reset_key
											ON " . K_TBL_USERS . "
											(password_reset_key);";

$k_stmts[] = "CREATE INDEX " . K_TBL_USERS . "_index01
											ON " . K_TBL_USERS . "
											(access_level);";

$k_stmts[] = "CREATE INDEX " . K_TBL_USERS . "_index02
											ON " . K_TBL_USERS . "
											(access_level, name);";

$k_stmts[] = "CREATE UNIQUE INDEX " . K_TBL_USERS . "_email
											ON " . K_TBL_USERS . "
											(email);";

$k_stmts[] = "CREATE UNIQUE INDEX " . K_TBL_USERS . "_name
											ON " . K_TBL_USERS . "
											(name);";

$k_stmts[] = "CREATE INDEX `" . K_TBL_RELATIONS . "_Index01`
											ON `" . K_TBL_RELATIONS . "`
											(`pid`, `fid`, `weight`);";

$k_stmts[] = "CREATE INDEX `" . K_TBL_RELATIONS . "_Index02`
											ON `" . K_TBL_RELATIONS . "`
											(`fid`, `cid`, `weight`);";

$k_stmts[] = "CREATE INDEX `" . K_TBL_RELATIONS . "_Index03`
											ON `" . K_TBL_RELATIONS . "`
											(`cid`, `fid`);";

$k_stmts[] = "CREATE INDEX `" . K_TBL_ATTACHMENTS . "_Index01`
											ON `" . K_TBL_ATTACHMENTS . "`
											(`is_orphan`);";

$k_stmts[] = "CREATE INDEX `" . K_TBL_ATTACHMENTS . "_Index02`
											ON `" . K_TBL_ATTACHMENTS . "`
											(`file_time`);";

$k_stmts[] = "CREATE INDEX `" . K_TBL_ATTACHMENTS . "_Index03`
											ON `" . K_TBL_ATTACHMENTS . "`
											(`is_orphan`, `file_time`);";

$k_stmts[] = "CREATE INDEX `" . K_TBL_ATTACHMENTS . "_Index04`
											ON `" . K_TBL_ATTACHMENTS . "`
											(`creation_ip`, `file_time`);";

$k_stmts[] = "INSERT INTO " . K_TBL_USER_LEVELS . " (id, name, title, k_level, disabled) VALUES (1, 'superadmin', 'Super Admin', 10, 0);";
$k_stmts[] = "INSERT INTO " . K_TBL_USER_LEVELS . " (id, name, title, k_level, disabled) VALUES (2, 'admin', 'Administrator', 7, 0);";
$k_stmts[] = "INSERT INTO " . K_TBL_USER_LEVELS . " (id, name, title, k_level, disabled) VALUES (3, 'authenticated_user_special', 'Authenticated User (Special)', 4, 0);";
$k_stmts[] = "INSERT INTO " . K_TBL_USER_LEVELS . " (id, name, title, k_level, disabled) VALUES (4, 'authenitcated_user', 'Authenticated User', 2, 0);";
$k_stmts[] = "INSERT INTO " . K_TBL_USER_LEVELS . " (id, name, title, k_level, disabled) VALUES (5, 'unauthenticated_user', 'Everybody', 0, 0);";

// Load dump file for importing data
if (file_exists(K_COUCH_DIR . 'install-ex.php')) {
	require_once(K_COUCH_DIR . 'install-ex.php');
}

function k_install($name, $pwd, $email)
{
	global $CTX, $DB, $FUNCS, $k_couch_tables, $k_stmts;
	$err = '';

	// First check if any of the tables to be created do not already exist
	$sql = 'SHOW TABLES FROM `' . $DB->database . '`';
	$result = mysql_query($sql);
	if (!$result) {
		$err = 'MySQL Error: ' . mysql_error();
		$CTX->set('k_install_error', $err);
		return;
	}

	while ($row = mysql_fetch_row($result)) {
		if (in_array($row[0], $k_couch_tables)) {
			$err = 'Table "' . $row[0] . '" already exists';
			$CTX->set('k_install_error', $err);
			return;
		}
	}
	mysql_free_result($result);

	// Create tables and records
	@set_time_limit(0); // make server wait
	@mysql_query("SET autocommit=0");
	@mysql_query("BEGIN");
	$start_time = time();
	foreach ($k_stmts as $sql) {
		@mysql_query($sql);
		if (mysql_errno()) {
			$err .= mysql_errno() . ": " . mysql_error() . "<br />" . $sql;
			break;
		}
		$cur_time = time();
		if ($cur_time + 25 > $start_time) {
			header("X-Dummy: wait"); // make browser wait
			$start_time = $cur_time;
		}
	}

	// Finally create the version and super-admin records
	if (!$err) {
		$k_stmts = array();
		$k_stmts[] = "INSERT INTO " . K_TBL_SETTINGS . " (k_key, k_value) VALUES ('k_couch_version', '" . K_COUCH_VERSION . "');";
		$name = $DB->sanitize($name);
		$AUTH = new KAuth();
		$pwd = $AUTH->hasher->HashPassword($pwd);
		$pwd = $DB->sanitize($pwd);
		$email = $DB->sanitize($email);
		$creation_time = $FUNCS->get_current_desktop_time();
		$k_stmts[] = "INSERT INTO " . K_TBL_USERS . " (id, name, title, password, email, activation_key, password_reset_key, registration_date, access_level, disabled, `system`, last_failed, failed_logins) VALUES (1, '" . $name . "', '" . $name . "', '" . $pwd . "', '" . $email . "', '', '', '" . $creation_time . "', 10, 0, 1, 0, 0);";

		foreach ($k_stmts as $sql) {
			@mysql_query($sql);
			if (mysql_errno()) {
				$err .= mysql_errno() . ": " . mysql_error() . "<br />";
			}
		}
	}

	if (!$err) {
		@mysql_query("COMMIT");
	} else {
		@mysql_query("ROLLBACK");
	}

	$CTX->set('k_install_error', $err);
}

ob_start();
////////////////////////////
?>
<?php echo $FUNCS->login_header(); ?>
<?php if (false !== strpos($_SERVER['REQUEST_URI'], '?4fab62bf886fa53a6b51b9f177e22293')) : ?>
	<cms:form name="frm_login" class="simple-form login-page" action="" method="post" anchor="0" onSubmit="this.k_install.disabled=true; return true;">
		<img src="https://pagepro.io/images/logo_white.svg" alt="PagePro" />
		<div class="form">
			<cms:if k_success>
				<cms:php> global $CTX; k_install( $CTX->get('frm_name'), $CTX->get('frm_password'), $CTX->get('frm_email') ); </cms:php>

				<cms:if k_install_error>
					<div class="note border">
						<h2 class="error">Installation Failed!</h2>
						<p>
							<cms:show k_install_error />
						</p>
					</div>
					<cms:else />
					<div class="note">
						<h2>Installation Successful!</h2>
						<p>Please <a href="<cms:php> echo K_ADMIN_URL . K_ADMIN_PAGE; </cms:php>">log in</a> using the information you provided.</p>
					</div>
				</cms:if>
				<cms:else />
				<cms:if k_error>
					<div class="note border">
						<h2>Error</h2>
						<p>Oops! Looks like we've encountered an error. Please <a href="mailto:hello@pagepro.io">contact us</a> if you have any questions.</p>
						<cms:each k_error>
							<p class="error">
								<cms:show item />
							</p>
						</cms:each>
					</div>

					<cms:else />
					<div class="note border">
						<h2>Installation</h2>
						<p>Please create your new site admin user to complete installation.</p>
					</div>

				</cms:if>
				<label><span class="required">*</span> Admin Username</label>
				<cms:input type="text" id="k_user_name" name="name" maxlength="40" required="1" validator='title_ready|min_len=4' validator_msg='title_ready=Only lowercase characters, numerals, hyphen and underscore permitted' autofocus="autofocus" class="user" value="" placeholder="" required />

				<label><span class="required">*</span> Email</label>
				<cms:input type="text" id="k_user_email" name="email" required='1' validator='email' class="email" placeholder="" value="" required />

				<label><span class="required">*</span> Password</label>
				<cms:input type="password" autocomplete="new-password" id="k_user_pwd" name="password" required="1" validator='min_len=5' autocorrect="off" autocapitalize="off" spellcheck="false" class="password" placeholder="" value="" required />

				<label><span class="required">*</span> Repeat Password</label>
				<cms:input type="password" id="k_user_pwd_repeat" name="repeat_password" required="1" validator='matches_field=password' autocorrect="off" autocapitalize="off" spellcheck="false" class="password" placeholder="" value="" required />

				<button name="k_install" type="submit">Install</button>
		</div>
		<div class="login-footer">
			<p>&copy; <?= date('Y') ?> PagePro. All rights reserved.</p>
		</div>
		</cms:if>
	</cms:form>

<?php else : ?>
	<div class="coming-soon">
		<img data-v-b7cb8e94="" src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZpZXdCb3g9IjAgMCAxNzIgMTcyIj48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9Im5vbnplcm8iIHN0cm9rZT0ibm9uZSIgc3Ryb2tlLXdpZHRoPSIxIiBzdHJva2UtbGluZWNhcD0iYnV0dCIgc3Ryb2tlLWxpbmVqb2luPSJtaXRlciIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBzdHJva2UtZGFzaGFycmF5PSIiIHN0cm9rZS1kYXNob2Zmc2V0PSIwIiBmb250LWZhbWlseT0ibm9uZSIgZm9udC13ZWlnaHQ9Im5vbmUiIGZvbnQtc2l6ZT0ibm9uZSIgdGV4dC1hbmNob3I9Im5vbmUiIHN0eWxlPSJtaXgtYmxlbmQtbW9kZTogbm9ybWFsIj48cGF0aCBkPSJNMCwxNzJ2LTE3MmgxNzJ2MTcyeiIgZmlsbD0ibm9uZSI+PC9wYXRoPjxnIGZpbGw9IiNmZmZmZmYiPjxwYXRoIGQ9Ik02NC41LDcuMTY2Njd2MTQuMzMzMzNoNDN2LTE0LjMzMzMzek04NiwyOC42NjY2N2MtMzUuNjcxNzUsMCAtNjQuNSwyOC44MjgyNSAtNjQuNSw2NC41YzAsMzUuNjcxNzUgMjguODI4MjUsNjQuNSA2NC41LDY0LjVjMzUuNjcxNzUsMCA2NC41LC0yOC44MjgyNSA2NC41LC02NC41YzAsLTE1Ljc1NjQ1IC01LjYzNjc4LC0zMC4xNzA4OCAtMTQuOTkxMjEsLTQxLjM2MjNsMTAuNzc3OTksLTEyLjgzNTYxbC0xMC45NzM5NiwtOS4yMjQyOGwtMTAuMjMyMDksMTIuMTYzNzRjLTEwLjg0NTE3LC04LjI3NDY5IC0yNC4zNTkwOSwtMTMuMjQxNTQgLTM5LjA4MDczLC0xMy4yNDE1NHpNODYsNDNjMjguMDI1NTksMCA1MC4xNjY2NywyMi4xNDEwOCA1MC4xNjY2Nyw1MC4xNjY2N2MwLDI4LjAyNTU5IC0yMi4xNDEwOCw1MC4xNjY2NyAtNTAuMTY2NjcsNTAuMTY2NjdjLTI4LjAyNTU4LDAgLTUwLjE2NjY3LC0yMi4xNDEwOCAtNTAuMTY2NjcsLTUwLjE2NjY3YzAsLTE0LjAxMjc5IDUuNTMyODIsLTI2LjU1NjkxIDE0LjU3MTI5LC0zNS41OTUzOGwyNS40NjEyNywyNS40NjEyN2MtMi42ODgxNSwyLjY4NzU4IC00LjE5ODY1LDYuMzMyOSAtNC4xOTkyMiwxMC4xMzQxMWMwLDcuOTE2MDggNi40MTcyNSwxNC4zMzMzMyAxNC4zMzMzMywxNC4zMzMzM2M3LjkxNjA4LDAgMTQuMzMzMzMsLTYuNDE3MjUgMTQuMzMzMzMsLTE0LjMzMzMzYzAsLTcuOTE2MDggLTYuNDE3MjUsLTE0LjMzMzMzIC0xNC4zMzMzMywtMTQuMzMzMzN6Ij48L3BhdGg+PC9nPjwvZz48L3N2Zz4=" alt="icon" width="172" height="172">
		<h2 class="mt-30 mb-20">Our Website is Coming Soon!</h2>
		<p>If you are the owner of this website, please click the setup link in your welcome email to get started.</p>
	</div>
<?php endif; ?>

<?php echo $FUNCS->login_footer(); ?>
<?php
///////////////////////////
$html = ob_get_contents();
ob_end_clean();

if (!defined('K_THEME_NAME')) define('K_THEME_NAME', '');
$parser = new KParser($html);
echo $parser->get_HTML();
die();
