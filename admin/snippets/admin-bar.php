<cms:if k_user_access_level ge '7'>
  <div class="admin-bar navbar-fixed-top y-middle">
    <div class="container">
      <div class="row">
        <div class="col-md-12 y-middle">
          <cms:form method='post' anchor='0'>
            <cms:if k_success>
              <cms:if "<cms:get_session 'inline_edit_on' />">
                <cms:delete_session 'inline_edit_on' />
                <cms:else />
                <cms:set_session 'inline_edit_on' value='1' />
              </cms:if>
              <cms:redirect k_page_link />
            </cms:if>

            <cms:if "<cms:get_session 'inline_edit_on' />">
              <cms:input name='submit' type="submit" value="Disable Edit Mode" class="edit-enabled" />
              <cms:else />
              <cms:input name='submit' type="submit" value="Enable Edit Mode" class="edit-disabled" />
            </cms:if>
          </cms:form>
          <a href="<cms:admin_link />" target="_blank">View Admin</a>
          <a href="<cms:show k_logout_link />">Logout</a>
        </div>
      </div>
    </div>
  </div>
</cms:if>
